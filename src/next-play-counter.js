const DEBUG = false

class NextPlayCounter {
    static UPDATE_INTERVAL_MS = 100
    updateTimeoutId = null
    shouldBeUpdating = false
    numberFormater = new Intl.NumberFormat('en-GB', { maximumFractionDigits: 1, minimumFractionDigits: 1 })
    
    constructor() {
        this.nextPlayCounterDisplay = document.querySelector('#next-sound-counter')
    }

    startUpdating() {
        DEBUG && console.log('startUpdating')
        this.shouldBeUpdating = true
        this.startUpdatingHelper()
    }

    startUpdatingHelper() {
        DEBUG && console.log('startUpdatingHelper', this.shouldBeUpdating)
        if (!this.shouldBeUpdating) return
        this.#updateDisplay()
        this.updateTimeoutId = setTimeout(() => this.startUpdatingHelper(), NextPlayCounter.UPDATE_INTERVAL_MS)
    }

    #updateDisplay() {
        DEBUG && console.log('updateDisplay')
        if (!estimatedNextSound) {
            this.nextPlayCounterDisplay.textContent = 'never'
            return
        }
        const timeToGoMs = estimatedNextSound - Date.now()
        const timeToGoS = timeToGoMs / 1000
        this.nextPlayCounterDisplay.textContent = `${this.numberFormater.format(timeToGoS)}s`
    }

    stopUpdating() {
        DEBUG && console.log('stopUpdating')
        clearTimeout(this.updateTimeoutId)
        this.shouldBeUpdating = false
        this.updateTimeoutId = null
    }
}
