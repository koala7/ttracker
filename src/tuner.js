// code from tinnitracks

(function() {
    var root;
    
    this.Tuner = (function() {
        function Tuner() {
            var listener;
            this.playing = false;
            this.whobbling = false;
            this.stopping = false;
            this.atVolumeChange = false;
            this.atFadeIn = false;
            this.activeBtn = void 0;
            if (typeof AudioContext !== "undefined") {
                if (this.audioCtx == null) {
                    this.audioCtx = new AudioContext();
                }
            } else if (typeof webkitAudioContext !== "undefined") {
                this.audioCtx = new webkitAudioContext();
            } else {
                console.log("Browser hat keine WebAudioAPI");
            }
            listener = this.audioCtx.listener;
            listener.setOrientation(0, 0, -1, 0, 1, 0);
            listener.setPosition(0, 0, 0);
            this.pannerNode = this.audioCtx.createPanner();
            this.pannerNode.setOrientation(0, 0, 0);
            this.pannerNode.setPosition(0, 0, 1);
            this.pannerNode.connect(this.audioCtx.destination);
            this.gainNode = this.audioCtx.createGain();
            this.gainNode.gain.value = 0.0;
            this.gainNode.connect(this.pannerNode);
            this.actualFreq = 500;
            this.currentVolume = 0.0;
        }
        
        Tuner.prototype.start = function(volume, elem_id) {
            var lfo, lfo_gain, now, that, whobGain;
            if (this.playing || this.stopping) {
                return;
            }
            this.playing = true;
            this.currentVolume = volume;
            this.oscillator = this.audioCtx.createOscillator();
            this.oscillator.type = "sine";
            this.oscillator.frequency.value = this.actualFreq;
            this.oscillator.connect(this.gainNode);
            if (this.whobbling) {
                lfo = this.audioCtx.createOscillator();
                lfo.type = "sine";
                lfo.frequency.value = 5;
                whobGain = this.actualFreq * 5.0 / 100.0;
                lfo_gain = this.audioCtx.createGain();
                lfo_gain.gain.value = whobGain;
                lfo.connect(lfo_gain);
                lfo_gain.connect(this.oscillator.frequency);
                lfo.start(0);
            }
            this.activeBtn = elem_id;
            if (this.activeBtn) {
                matchingGUI.highlightPlay(this.activeBtn);
            }
            now = this.audioCtx.currentTime;
            this.atFadeIn = true;
            this.gainNode.gain.setValueAtTime(0.0, now + 0.01);
            this.oscillator.start(now + 0.02);
            this.gainNode.gain.setValueAtTime(0.0, now + 0.03);
            this.gainNode.gain.linearRampToValueAtTime(volume, now + 0.2);
            that = this;
            return setTimeout(function() {
                return that.atFadeIn = false;
            }, 200);
        };
        
        Tuner.prototype.stop = function() {
            var now, that;
            if (this.playing && !this.stopping) {
                this.stopping = true;
                now = this.audioCtx.currentTime;
                if (this.atVolumeChange) {
                    now += 0.3;
                }
                if (this.atFadeIn) {
                    now += 0.3;
                }
                this.gainNode.gain.setValueAtTime(this.currentVolume, now + 0.01);
                this.gainNode.gain.linearRampToValueAtTime(0.0, now + 0.45);
                this.oscillator.stop(now + 0.5);
                if (this.activeBtn) {
                    matchingGUI.highlightStop(this.activeBtn);
                }
                that = this;
                return setTimeout(function() {
                    that.stopping = false;
                    that.playing = false;
                    return that.whobbling = false;
                }, 750);
            }
        };
        
        Tuner.prototype.setFrequency = function(newFreq) {
            this.actualFreq = newFreq;
            if (this.playing && !this.stopping) {
                return this.oscillator.frequency.value = newFreq;
            }
        };
        
        Tuner.prototype.changeVolume = function(newVolume) {
            var newVolumeDb, now, that;
            newVolumeDb = this.linearToDb(newVolume);
            if (!this.stopping) {
                this.atVolumeChange = true;
                now = this.audioCtx.currentTime;
                this.currentVolume = newVolumeDb;
                this.gainNode.gain.exponentialRampToValueAtTime(this.currentVolume, now + 0.333);
                that = this;
                return setTimeout(function() {
                    return that.atVolumeChange = false;
                }, 300);
            }
        };
        
        Tuner.prototype.setWhobbling = function() {
            return this.whobbling = true;
        };
        
        Tuner.prototype.setPanToLR = function() {
            if (!this.playing) {
                return this.pannerNode.setPosition(0, 0, 1);
            }
        };
        
        Tuner.prototype.setPanToL = function() {
            if (!this.playing) {
                return this.pannerNode.setPosition(-3, 0, 0);
            }
        };
        
        Tuner.prototype.setPanToR = function() {
            if (!this.playing) {
                return this.pannerNode.setPosition(3, 0, 0);
            }
        };
        
        Tuner.prototype.play = function(frequency, volume, whob, elem_id) {
            var volumeDb;
            if (whob == null) {
                whob = false;
            }
            this.setFrequency(frequency);
            if (whob) {
                this.setWhobbling();
            }
            volumeDb = this.linearToDb(volume);
            return this.start(volumeDb, elem_id);
        };
        
        Tuner.prototype.linearToDb = function(s) {
            var dbStart;
            dbStart = 90;
            s = s * dbStart - dbStart;
            return Math.pow(10, s / 20);
        };
        
        return Tuner;
        
    })();
    
    root = typeof exports !== "undefined" && exports !== null ? exports : window;
    
    root.Tuner = Tuner;
    
}).call(this);