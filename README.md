# ttracker

The _ttracker_ is a tool to help coping with Tinnitus.

Deployed at https://ttracker.simon-lenz.de/.


> The ttracker was created to help suppress tinnitus. The idea came about during a frequency determination of the tinnitus. The sounds played during this procedure seemed to temporarily stop the ringing at times. The ttracker enables you to play configurable sounds repeatedly.
>
> Please be cautious while using, and start with lower volumes.
>
> The application was not created by medical professionals.
>
> Use at your own risk! 

taken from https://ttracker.simon-lenz.de/about.html.