// wraps tinnitracks tuner
const DIRECTION = {
    LEFT: 'L',
    RIGHT: 'R',
    LR: 'LR',
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const tuner = new Tuner()
let isPlayingRepeatedly = false
let repeatTimeoutId = null
let estimatedNextSound = null
let firstPlayOfCurrentRepeatLoop = null

async function play({
    direction = DIRECTION.LR,
    durationMs = 1000,
    frequency = 1000,
    volume = 0.7,
} = {}) {
    console.log('playing tune')

    if (direction === DIRECTION.LEFT) {
        tuner.setPanToL()
    }
    else if (direction === DIRECTION.RIGHT) {
        tuner.setPanToR()
    }
    else if (direction === DIRECTION.LR) {
        tuner.setPanToLR()
    }

    tuner.play(frequency, volume, false, null)
    await timeout(durationMs)
    tuner.stop()
}

async function playRepeatedly({
    delayMs = 2000,
    lengthMs = 30 * 60 * 1000,
    playOptions,
}) {
    isPlayingRepeatedly = true
    firstPlayOfCurrentRepeatLoop = Date.now()
    playRepeatedlyHelper({ delayMs, lengthMs, playOptions })
}

async function playRepeatedlyHelper({
    delayMs,
    lengthMs,
    playOptions,
}) {
    await play(playOptions)
    if (!isPlayingRepeatedly) return
    if (firstPlayOfCurrentRepeatLoop + lengthMs <= Date.now()) {
        stopCurrentPlay()
        return
    }
    console.log('Schedule new play in ', delayMs)
    estimatedNextSound = Date.now() + delayMs
    repeatTimeoutId = setTimeout(() => {
        playRepeatedlyHelper({ delayMs, lengthMs, playOptions })
    }, delayMs)
}

function isPlaying() {
    return isPlayingRepeatedly || tuner.playing || tuner.stopping
}

function stopCurrentPlay() {
    console.log('Stop current play!')
    if (isPlayingRepeatedly) {
        console.log('Clear repeated play schedule')
        clearTimeout(repeatTimeoutId)
        repeatTimeoutId = null
        isPlayingRepeatedly = false
        estimatedNextSound = null
        firstPlayOfCurrentRepeatLoop = null
        playStopButton.textContent = 'Play'
        setTimeout(() => nextPlayCounter.stopUpdating(), 200)
    }
    tuner.stop()
}