class History {
    static KEY = 'history'

    constructor() {
        this.#assureUpToDateVersion()    
    }
    
    #assureUpToDateVersion() {
        const historyObj = JSON.parse(localStorage.getItem(History.KEY))                    
        if (!historyObj || historyObj.version === 1) {
            return
        }

        const entries = historyObj ?? []
        const historyObjectInUpToDateVersion = {
            version: 1,
            entries,
        }
        localStorage.setItem(History.KEY, JSON.stringify(historyObjectInUpToDateVersion))
    }

    #getEntries() {
        return JSON.parse(localStorage.getItem(History.KEY))?.entries ?? []
    }

    #storeEntries(entries) {
        localStorage.setItem(History.KEY, JSON.stringify({
            version: 1,
            entries,
        }))
    }
    
    addEntry(options) {
        console.log('add to hist', options)
        const lastEntry = this.getLastEntry()
        if (
            options.volume === lastEntry?.volume &&
            options.frequency === lastEntry?.frequency &&
            options.durationMs === lastEntry?.durationMs &&
            options.direction === lastEntry?.direction
        ) {
            console.debug('Same config played again. Updating history.')
            const entries = this.#getEntries()
            entries[0].time = new Date().toISOString()
            this.#storeEntries(entries)
            return
        }

        const newHistory = [
            {
                time: new Date().toISOString(),
                ...options,
            },
            ...this.#getEntries(),
        ]
        this.#storeEntries(newHistory)
    }

    getLastEntries() {
        const AMOUNT_OF_ENTRIES = 100
        return this.#getEntries().slice(0, AMOUNT_OF_ENTRIES)
    }

    getLastEntry() {
        return this.#getEntries()[0] ?? null
    }
}